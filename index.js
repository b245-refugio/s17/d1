// console.log("hellow")

// [SECTION] Functions
	// Functions in javascript are line/blocks of codes that tell our device/application to perform a specific task when called/invoked.
	// it prevents repeating lines.blocks of codes that perform the same task function.
	
	// Function Declarations
		// function statement is the definition of a function.

	/*
		Syntax:
			function functionName(){
				code block (statement)
			}

		- function keyword - used to define a javascript function
		- functionName - name of the function, which will be used to call/invoked the function
		- function block({}) - indicates the function body.

	*/

function printName(){
	console.log("My name is Jimmy");
}

// Function Invocation
	//This run/execute the code block inside the function.
	printName();

	declaredFunctions(); // we cannot invoked a function that we have not declared/defined yet.

// [SECTION] Function Declarations vs Functions Expressions

	// Function Declaration
		// function can be created by using function keyword and adding a function name.
		// "saved for later used"
		// Declared functions can be "hoisted", as long a function has been defined.
			// Hoisting is JS behavior for certain variables (var) and functions to run ir use them before their declaration.

	function declaredFunctions(){
		console.log("Hello World from declaredFunctions()");
	}

	//  Function Expression
		// A function can also be stored in a variable.

		/*
			Syntax let/const variableName = function(){
				//code block (statement)
			}

			- function(){} - Anonymous function, a function without a name.
		*/

	//variableFunction(); // error - function expression, being stored in a let/const variable, cannot be hoisted.

	let variableFunction = function(){
		console.log("Hellow Again!");
	}

	variableFunction();

	// We create also function expression with a named function.

	let funcExpression = function funcName(){
		console.log("Hello from the other side.");
	}

	// funcName(); //funcName is not defined
	funcExpression(); // to invoke the function, we invoked it by its "variable name" and not by its function name.

	// Reassign declared functions and function expression to a new anonymous function.

	declaredFunctions = function(){
		console.log("updated declaredFunctions");

	}
	declaredFunctions();

	funcExpression = function (){
		console.log("updated funcExpression");
	}
	funcExpression();

	// we cannot re-assign a function expression initialized with const.

	const constantFunc = function(){
		console.log("Initialized with const");
	}

	constantFunc();


	// This will result to reassignment error
	// constantFunc = function(){
	// 	console.log("cannot be reassign");
	// }

	// constantFunc();

// [SECTION] Function Scoping
	/*
		Scope is the accessibbility (visibility) of variables.

		JavaScript Variables has 3 types of scope:
		1. global scope
		2. local/block scope
		3. function scope

	*/

	// Global Scope
		// variable can access any where from the program.

	let globalVar = "Mr. WordWide";

	// Local Scope
		// Variables declared inside a curly bracket ({}) can only be accessed locally.

	// console.log(localVar);

	{
		let localVar = "Armando Perez";
		// var localVar = "Armando Perez";
	}

	console.log(globalVar);
	//console.log(localVar); // result in error. cannot be accessed outside it code block

	// Function Scope
		// Each Function creates a new scope.
		// Variables defined inside a function are not possible accessible from outside the function.

	function showNames(){
		// Function scope Variable
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Joey";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}

	showNames();

		// console.log(functionVar);
		// console.log(functionConst);
		// console.log(functionLet);

	function myNewFunction(){
		let name = "Jane";
		
		function nestedFunc(){
			let nestedName = "John";
			console.log(name);
		}
		// console.log(nestedName);
		nestedFunc();
	}

	myNewFunction();
	// nestedFunc(); // result to an error.

// [SECTION] Using alert() and prompt()
	// alert() allows us to show small window at the top of our browser page to show information to our users.
	
	//alert("Hello world"); // this will run imediately when the page reloads.

	// You can use an alert() to show a message to a user from a later function invocation

	// function showSampleAlert(){
	// 	alert("Hello, User");
	// }

	// showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed.");

	// Notes on the use of alert();

		// Show only an alert() for short dialogs/message to the user.
		// Do not overuse alert() beacause the program has to wait for it to be dismessed before it continues.

	// prompt() allows us to show a small window at the top of the browser to gather user input.
	// The input from the prompt() will returned as a "String" once the user dismissed the window. 

	/*
		Syntax:
		let/const variableName = prompt("<dialogInString>");

	*/

	// let name = prompt("Enter your Name: ");
	// let age = prompt("Enter your age: ");

	// console.log(typeof age);
	// console.log("Hello, I am "+name+", and I am "+age+" years old.")

	// let sampleNullPrompt = prompt("Do Not Enter Anything");
	// console.log(sampleNullPrompt);
	// prompt() return an "Empty string" ("") when the is no user input and we have clicked okkay.. or "null" if the user cancels the prompt.

	function printWelcomeMessage(){
		let name = prompt("Enter your Name: ");
		console.log("Hello, "+name+"! Welcome to my page");
	}

	printWelcomeMessage();

	// [SECTION] Function Naming Conventions
		// Function names should be definitive of the task it will perform. it usually contains a verb.
		// 
	function getCourses(){
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}

	getCourses();

	// 2. Avoid generic names to avoid confusion withing your code

	function get(){
		let name = "Jamie";
		console.log(name);
	}

	get();

	// 3. Avoid pointless and inappropriate function names, example: foo, bar. (Metasyntactic variable - placeholder variable).

	function foo(){
		console.log(25%5);
	}

	foo();